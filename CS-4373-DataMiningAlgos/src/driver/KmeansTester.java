package driver;

import java.io.File;

import algorithms.Kmeans;

public class KmeansTester {
	/*
	 * for testing k-means related things
	 * Will probably be deleted once finished
	 */
	public static void main(String[] args) {
		Kmeans km = new Kmeans(new File("src/datasets/iris.txt"), 3, -1);
		
		System.out.printf("centers found:\n");
		System.out.printf("Sepal:\n");
		for(int j = 0; j < km.k; j++)
			System.out.printf("%f, %f\n", km.centers[j].x, km.centers[j].y);
		System.out.printf("Petal:\n");
		for(int j = 0; j < km.k; j++)
			System.out.printf("%f, %f\n", km.petalCenters[j].x, km.petalCenters[j].y);
	}

}
