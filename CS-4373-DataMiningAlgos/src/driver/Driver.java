package driver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import controllers.MenuController;
//import controllers.MasterController;
//import controllers.MenuController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Driver extends Application
{
	private static final Logger logger = LogManager.getLogger(Driver.class);
	
	public void init() throws Exception {
		super.init();
		// create gateway
		logger.info("Calling init");
		//MasterController.getInstance();
	}
	
	public void stop() throws Exception {
		super.stop();
		// close the gateway
		logger.info("Calling stop");
		//MasterController.getInstance().close();
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{
		logger.info("Loading application...");
		
		

		//Parent root = FXMLLoader.load(getClass().getResource("assign2.fxml"));
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/assign2.fxml"));
		loader.setController( new MenuController() );
		Parent root = loader.load();
		
		//MasterController.getInstance().setRootPane( (BorderPane) root);
		
		Scene scene = new Scene(root); // create a scene from the FXML stuff
		
		primaryStage.setTitle("Data Mining Algorithm Implementation"); // title of the software
		primaryStage.setScene(scene); // specify the scene to be used with the stage
		primaryStage.show(); // set visibility to true, show Window
		
		logger.info("Loading complete, application running");
	}
	
	public static void main(String[] args)
	{
		launch( args );

	}
	

}
