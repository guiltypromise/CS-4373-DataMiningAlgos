/**
 * MenuController.java
 * @author Jason Martin
 * 
 * Contains backend logic that translates Scene logic selections
 * from FXML Views.
 */

package controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import algorithms.Apriori;

import algorithms.Kmeans;
import algorithms.NaiveBayes;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.collections.ObservableList;


public class MenuController implements Initializable 
{
	// Logging manager object for this class
	private static final Logger logger = LogManager.getLogger(MenuController.class);

	@FXML private MenuBar menuBar; // the menuBar from the FXML
	
	/* Menu Items */
	@FXML private MenuItem menuItemExit; 
	@FXML private MenuItem menuItemHome; 
	@FXML private Menu menuQuitButton;
	@FXML private Button runButton;
	@FXML private TextArea textBoxReport;
	@FXML private ComboBox comboBoxAlgo;
	@FXML private ComboBox comboBoxData;
	
	/* Controller vars */
	
	private NaiveBayes myBayes;
	private Kmeans kMeans;
	private Apriori apriori;
	private static final String irisSet = "src/datasets/iris.arff";
	private static final String irisTextSet = "src/datasets/iris.txt";
	private static final String irisTrainSet = "src/datasets/irisTrain.txt";
	private static final String irisClassifySet = "src/datasets/irisClassify.txt";
	private static final String irisTrainSet2 = "src/datasets/irisTrain2.txt";
	private static final String irisClassifySet2 = "src/datasets/irisClassify2.txt";
	//private static final String chessData = "src/datasets/chess.dat";
	private static final String retailData = "src/datasets/retail.dat";
	
	
	public MenuController()
	{
		super();
		File file = new File(irisTextSet);
		//Init comboBoxes
		comboBoxAlgo = new ComboBox();
		comboBoxData = new ComboBox();
		
		// Init models
		myBayes = new NaiveBayes();
		kMeans = new Kmeans( file, 3, -1);
		apriori = new Apriori( retailData );


		// Init datasets
	}
	
	
	@FXML private void handleRunButtonAction(ActionEvent event) throws IOException
	{
		logger.info("RUN button pressed");
		try {
			// attempt to get algo choice
			Object choice = comboBoxAlgo.getSelectionModel().selectedItemProperty();
			//logger.info(choice.toString());


			if ( choice.toString().contains("Naive Bayes") )
			{
				logger.info("Loading Naive Bayes Implementation");

				Object data = comboBoxData.getSelectionModel().selectedItemProperty();

				if( data.toString().contains("1."))
				{
					logger.info("Setting 1. Iris Data set");
					myBayes = new NaiveBayes();
					//myBayes.setDataFile( irisTextSet );
					myBayes.setDataFile( irisTrainSet );
					try {
						myBayes.train();
						myBayes.classify( irisClassifySet );
					} catch ( Exception e )
					{
						e.printStackTrace();
					}

					textBoxReport.setText(myBayes.outputTrainingData() + myBayes.outputValidationData());			
				}
				else if ( data.toString().contains("2."))
				{
					myBayes = new NaiveBayes();
					logger.info("Setting 2. Iris Data 50:50");
					myBayes.setDataFile( irisTrainSet2 );
					try {
						myBayes.train();
						myBayes.classify( irisClassifySet2 );

					} catch ( Exception e )
					{
						e.printStackTrace();
					}

					textBoxReport.setText(myBayes.outputTrainingData()+myBayes.outputValidationData());	

				}
				else
				{
					errorDialog("Please select a data set.");
				}

			}
			else if( choice.toString().contains("K"))
			{
				
				Object data = comboBoxData.getSelectionModel().selectedItemProperty();

				// TODO k-means operations and output for certain data sets
				
				kMeans.showGraphSepals();
				kMeans.showGraphSepalsCluster();
				kMeans.showGraphPetals();
				kMeans.showGraphPetalsCluster();
				
			}
			else if (choice.toString().contains("Apriori"))
			{
				logger.info("Loading Apriori Implementation");

				Object data = comboBoxData.getSelectionModel().selectedItemProperty();

				// TODO apriori operations and outputf
				if (data.toString().contains("3."))
				{
					logger.info("Loading retail.dat");
					try {
						textBoxReport.setText(apriori.printOutput());
					} catch (Exception e)
					{
						e.printStackTrace();
					}


				}
			}
			else
			{
				errorDialog("Please select an algorithm before running.");
			}
		} catch (Exception e) {
			//e.printStackTrace();
		}

	}
	
	
	/**
	 * Handles menu item actions the user selects from the menu.
	 * @param event The particular event selected.
	 * @throws IOException Throw an error if item can't be found matching logic.
	 */
	@FXML private void handleMenuAction(ActionEvent event ) throws IOException 
	{
		Object source = event.getSource();
		Object data = null;

		try {	

			
			// If the Exit menu item is selected
			if( source == menuItemExit ) {
				logger.info("Closing application");
				Platform.exit();
			} else {
				logger.warn("Unhandled menu action, defaulting to Home view");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("IO Menu Error: " + e.getMessage() );
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		ObservableList algoItems = comboBoxAlgo.getItems();
		
		algoItems.add( myBayes );
		algoItems.add( kMeans );
		algoItems.add( apriori );
		
		ObservableList dataItems = comboBoxData.getItems();
		
		dataItems.add("1. Iris Set (Bayes, K-Means)");
		dataItems.add("2. Iris Set 50:50 (Bayes)");
		dataItems.add("3. Retail.dat (Apriori)");
		
		menuBar.setFocusTraversable( true );
		
	}
	
	private void errorDialog(String message )
	{
		Alert alert = new Alert(AlertType.ERROR);

		alert.setTitle("Error");
		alert.setHeaderText( message );
		//alert.setContentText("Would you like to save?");
		
		ButtonType buttonTypeOK = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE );
		
		alert.getButtonTypes().setAll(buttonTypeOK);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeOK)
		{
			logger.info("Error confirmed");	
		}
		
		logger.error(message);
	}
} // end MenuController 
