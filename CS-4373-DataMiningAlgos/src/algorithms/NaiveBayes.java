/**
 * Naive Bayes classifier by Jason Martin
 */

package algorithms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import controllers.MenuController;
import models.BayesianNetwork;
import models.Factor;
import models.IrisClass;
import models.Variable;
import models.IrisObject;

public class NaiveBayes 
{
	private static final Logger logger = LogManager.getLogger(NaiveBayes.class);

	private String file;
	private String classifyFile;
	private BayesianNetwork bn;
	private Factor factor;
	private Variable variable;
	private ArrayList<IrisObject> irisData;
	private ArrayList<IrisObject> irisValidationData;
	private ArrayList<IrisObject> irisClassifieds;
	private HashMap<String, IrisClass> irisClassMap;
	

	public NaiveBayes()
	{
		super();
		file = "";
		classifyFile = "";
		bn = new BayesianNetwork();
		irisData = new ArrayList<IrisObject>();
		irisClassifieds = new ArrayList<IrisObject>();
		irisClassMap = new HashMap<String, IrisClass>();
		//this.file = sourcefile;
		//factor = new Factor();
		//variable = new Variable();
	}
	
	public void readFile()
	{
		System.out.println("Opening the file: " + this.file);
		File file = new File(this.file);
		Scanner scan = null;
		String line = "";
		irisData = new ArrayList<IrisObject>();
		try { // opening the file
			int iCount = 0;
			scan = new Scanner(new BufferedReader(new FileReader(this.file))).useDelimiter(",");
			while( scan.hasNextLine() )
			{
				double sW;
				double sL;
				double pW;
				double pL;
				String name;
				IrisObject iris = new IrisObject();
				sL = scan.nextDouble();
				iCount++;
				sW = scan.nextDouble();
				iCount++;
				pL = scan.nextDouble();
				iCount++;
				pW = scan.nextDouble();
				iCount++;
				name = scan.nextLine().substring(1);
				iCount++;

				iris.setSepalWidth( sW );
				iris.setSepalLength( sL );
				iris.setPetalWidth( pW );
				iris.setPetalLength( pL );

				iris.setClassName( name );
				//System.out.println( iris.getClassName() );
				
				irisData.add(iris);
			}
		} 
		catch (FileNotFoundException e) { // catch error
			System.out.println("ERROR: '" + this.file + "' cannot be found");
			//e.printStackTrace();
		}
		finally 
		{
			logger.info("FINISHED READING FILE: " + this.file);
			scan.close();
		}
	}
	
	public void readClassifyFile()
	{
		System.out.println("Opening the file: " + this.classifyFile);
		File file = new File(this.classifyFile);
		Scanner scan = null;
		String line = "";

		try { // opening the file
			int iCount = 0;
			irisValidationData = new ArrayList<IrisObject>();

			scan = new Scanner(new BufferedReader(new FileReader(this.classifyFile))).useDelimiter(",");
			while( scan.hasNextLine() )
			{
				double sW;
				double sL;
				double pW;
				double pL;
				String name;
				IrisObject iris = new IrisObject();
				sL = scan.nextDouble();
				iCount++;
				sW = scan.nextDouble();
				iCount++;
				pL = scan.nextDouble();
				iCount++;
				pW = scan.nextDouble();
				iCount++;
				name = scan.nextLine().substring(1);
				iCount++;

				iris.setSepalWidth( sW );
				iris.setSepalLength( sL );
				iris.setPetalWidth( pW );
				iris.setPetalLength( pL );

				iris.setClassName( name );
				//System.out.println( iris.getPetalWidth() );
				
				irisValidationData.add(iris);
			}
		} 
		catch (FileNotFoundException e) { // catch error
			System.out.println("ERROR: '" + this.classifyFile + "' cannot be found");
			//e.printStackTrace();
		}
		finally 
		{
			scan.close();
		}
	}

	public String outputTrainingData()
	{
		String out = "";
	    Collection c = irisClassMap.values();
	    Iterator itr = c.iterator();
	    IrisClass irisClass;
	    // for each class in the hash map
    	StringBuilder s = new StringBuilder();
    	s.append("\n\t\t  Training Model, Total = " + irisData.size() + "\n");
    	s.append("\t\t\t\t\t Mean \t\t Std Dev\n");
	    try {
		    while( itr.hasNext() ) // output data for each class
		    {
		    	irisClass = (IrisClass) itr.next(); // get reference from hash map
		    	s.append( String.format("%s \nCount = %s\n", irisClass.getName(), irisClass.getTotal() ) );
	
		    	s.append( String.format("\t %s\t\t %.3f \t\t %.4f\n"
		    			, "Sepal Length"
		    			, irisClass.getMeanSepalLength()
		    			, irisClass.getStdSepalLength() 
		    			));
		    	s.append( String.format("\t %s\t\t %.3f \t\t %.4f\n"
		    			, "Sepal Width"
		    			, irisClass.getMeanSepalWidth()
		    			, irisClass.getStdSepalWidth()
		    			) );
		    	s.append( String.format("\t %s\t\t %.3f \t\t %.4f\n"
		    			, "Petal Length"
		    			, irisClass.getMeanPetalLength()
		    			, irisClass.getStdPetalLength()
		    			) );
		    	s.append( String.format("\t %s\t\t %.3f \t\t %.4f\n"
		    			, "Petal Width"
		    			, irisClass.getMeanPetalWidth()
		    			, irisClass.getStdPetalWidth()
		    			) );
		    	s.append("\n");
		    }
	    }
	    catch (Exception e)
	    {
	    	e.printStackTrace();
	    }
//
//		for( int i = 0; i < irisData.size(); i++ )
//		{
//			//System.out.println( "Count: " + i );
//			out += irisData.get(i).toString() + "\n";
//		}
	    //out = s.toString();
		return s.toString();
	}

	public String outputValidationData()
	{
		int total = irisValidationData.size();
		int numCorrect = total;
		StringBuilder s = new StringBuilder();
		s.append("\n\t\t Validation Set, Total = " + irisValidationData.size() + "\n" );
		// for each validation record
		for( int i = 0; i < irisValidationData.size(); i++)
		{
			IrisObject x = irisValidationData.get(i);
			s.append(String.format("%.1f, %.1f, %.1f, %.1f\t\tPrediction => %s\n"					
					, x.getSepalLength()
					, x.getSepalWidth()
					, x.getPetalLength()
					, x.getPetalWidth()
					, x.getClassName()
					));
		}

		s.append("\n\t\t Classification Step \n");

		// for each classified iris object
		for( int i = 0; i < irisClassifieds.size(); i++)
		{
			IrisObject x = irisClassifieds.get(i);
			s.append(
					String.format("%.1f, %.1f, %.1f, %.1f = %s"
					, x.getSepalLength()
					, x.getSepalWidth()
					, x.getPetalLength()
					, x.getPetalWidth()
					, x.getClassifiedName()
					)
			);
			if( x.getClassName().equals(x.getClassifiedName()))
			{
				//s.append("\t\t Correct!\n");
				s.append("\n");
			}
			else
			{
				numCorrect--;
				s.append("\t\t X\t[ "+  x.getClassName() + " ]\n");
			}
			
		}
		s.append("--------------------------------------------------------------\n");
		s.append(String.format( "\tSummary:\n\t\tClassified %s out of %s correctly\n", numCorrect, total));
		
		return s.toString();
	}
	// Assumes that the data has been stored into the irisData instance member
	public void train() throws Exception
	{    
		logger.info("BEGIN TRAINING ON FILE: " + this.file);
		// PRE-PROCESS for training
		IrisClass irisClass = null;

	    // for each data record
		for( int i = 0; i < irisData.size(); i++ )
		{
			// if existing class, pull reference from HashMap
			if( irisClassMap.containsKey(irisData.get(i).getClassName()) )
			{
				irisClass = irisClassMap.get(irisData.get(i).getClassName());
			}
			else // make a new class for storing hash map table
			{
				//System.out.println("Storing: " + irisData.get(i).getClassName());
				irisClass = new IrisClass( irisData.get(i).getClassName() );
			}
			
			// add the data record for that class
			irisClass.addToCount();
			irisClass.addData( irisData.get(i));
			
			// save into the hash map
			irisClassMap.put(irisClass.getName(), irisClass );
		}
		
		//ArrayList<IrisClass> classes = irisClassMap.values();
	    Collection c = irisClassMap.values();
	    Iterator itr = c.iterator();
	    
	    // for each class in the hash map
	    while( itr.hasNext() ) // process data for each class
	    {
	    	irisClass = (IrisClass) itr.next(); // get reference from hash map
	    	irisClass.calculateMeans(); // process means
	    	irisClass.calculateStdDevs(); // process std devs
	    	
			// update into the hash map 
			irisClassMap.put(irisClass.getName(), irisClass );
	    }
		
		logger.info("FINISHED TRAINING NAIVE BAYES");
	    
	}
	
	public void classify(String filename) throws Exception
	{
		classifyFile = filename;
		readClassifyFile();
		ArrayList<IrisClass> classList = new ArrayList<IrisClass>();
		
		
		// Iris Setosa = P(sepalWidth | setosa) * ... * P(sepalWidth)
		// ...
		// ...

		IrisClass irisClass;
		IrisClass maxClass;
		// Get mean and std of each Class
		// for each record in the validation set
			// for each variable
				// use Probabilty Density Function
		IrisObject x;
		for( int j = 0; j < irisValidationData.size(); j++)
		{
			classList = new ArrayList<IrisClass>();
			x = irisValidationData.get(j);
			Collection c = irisClassMap.values();
			Iterator itr = c.iterator();
			while( itr.hasNext() )
			{
				irisClass = (IrisClass) itr.next();
				IrisClass irisSavedClass = new IrisClass(irisClass.getName()); 
				
				// calculate probability using PDF on all variables
				irisSavedClass.setProbability(
							(
							probDensityFunc( x.getSepalLength()
								, irisClass.getMeanSepalLength()
								, irisClass.getStdSepalLength())
							* 
							probDensityFunc( x.getSepalWidth()
									, irisClass.getMeanSepalWidth()
									, irisClass.getStdSepalWidth())
							*
							probDensityFunc( x.getPetalLength()
									, irisClass.getMeanPetalLength()
									, irisClass.getStdPetalLength())
							* 
							probDensityFunc( x.getPetalWidth()
									, irisClass.getMeanPetalWidth()
									, irisClass.getStdPetalWidth())
							*
							(0.33)
							)
						);
				// save to working classList
				classList.add(irisSavedClass);
			} // end while
			
			// initial max class
			maxClass = classList.get(0);
			
			// find the max class for the data record
			for( int i = 0; i < classList.size(); i++ )
			{
				//System.out.println(" BEFORE MAX P " + maxClass.getName() + " = " + maxClass.getProbability());

				if( classList.get(i).getProbability() > maxClass.getProbability()  )
				{
					//System.out.println("Found higher max = " + classList.get(i).getProbability() + " > " + maxClass.getProbability());
					//maxClass.setProbability(element.getProbability());
					//maxClass.setName(element.getName());
					maxClass = classList.get(i);
				}
				
				//System.out.println(" P " + element.getName() + " = " + element.getProbability());
				//System.out.println(" \tAFTER MAX P " + maxClass.getName() + " = " + maxClass.getProbability());

			}
			x.setClassifiedName( maxClass.getName() );
//			logger.info(String.format("Classifying %.1f, %.1f, %.1f, %.1f = %s"
//					, x.getSepalLength()
//					, x.getSepalWidth()
//					, x.getPetalLength()
//					, x.getPetalWidth()
//					, maxClass.getName()
//					));
			
			irisClassifieds.add(x);
		} // end for
	} // end classify()
	
	/**
	 * calculates the probability of a class given a variable x, class' variable mean, and class' variable std dev
	 * @param x
	 * @param mean
	 * @param std
	 * @return
	 */
	private double probDensityFunc(double x, double mean, double std )
	{
		double output = (double) 
						(
				Math.pow( Math.E, -(Math.pow((x-mean), 2)
							/ (2 * Math.pow(std, 2)))
						)
				
				/  
				
				Math.sqrt( (2 * Math.PI  * Math.pow(std, 2) )
						)
				);
		//output = round(output, 4);
		return output;
	}
	
	public void setDataFile(String filename)
	{
		file = filename;
		readFile();
	}
	
	public String toString()
	{
		return "Naive Bayes Implementation";
	}
	
	private static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
}
