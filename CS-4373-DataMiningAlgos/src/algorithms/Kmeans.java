package algorithms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;
import java.util.Random;

import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import models.Point;

public class Kmeans {
	public Point[] pointArray; // holds the data points
	public Point[] petalArray;
	public Point[] centers; // holds the centers
	public Point[] petalCenters;
	public int[] c; // List of the current cluster the pointArray point at location c[x] belongs to
	public int[] petalC;
	public String[] type;
	public int k; // number of clusters to be found
	private int iters; // number of iterations till k-means ends
	private int m; // number of data points
	private File f; // The file to be read
	private int MAX_POINTS = 32768; // maximum points allowed. Change as necessary
	private int MAX_CENTERS = 16; // maximum centers allowed. Change as necessary
	
	public Kmeans(File f, int k, int iters){ 
		this.f = f;
		this.k = k;
		this.iters = iters;
		m = 0;
		type = new String[MAX_POINTS];
		pointArray = new Point[MAX_POINTS]; 
		petalArray = new Point[MAX_POINTS];
		centers = new Point[k];
		petalCenters = new Point[k];
		c = new int[MAX_POINTS];
		petalC = new int[MAX_POINTS];
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		try {
			fileReader = new FileReader(f);
			bufferedReader = new BufferedReader(fileReader);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			String [] tokens;
			while ((line = bufferedReader.readLine()) != null) {
				// add the data points to pointArray
				if(!line.equals("")){ 
					tokens = line.split(",");
					//System.out.println("tokens: line "+ m + ": "  + tokens[0] + " " + tokens[1] );
					pointArray[m] = new Point(Double.parseDouble(tokens[0]),  Double.parseDouble(tokens[1]));
					petalArray[m] = new Point(Double.parseDouble(tokens[2]), Double.parseDouble(tokens[3]));
					type[m] = tokens[4];
					//System.out.println(tokens[2] + " " + tokens[3]);
					m++;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		// test if array has properly been assigned
		for(Point point: pointArray){
			if(point == null) break;
			System.out.println(point.x + ", " + point.y);
		}
		*/
		// generate random centers
		
		genCenters(centers, pointArray);
		genCenters(petalCenters, petalArray);
		// find the centers
		findCenters(pointArray, centers, c);
		findCenters(petalArray, petalCenters, petalC);
	}

	/*
	 * Set up random points to be the initial centroids
	 */
	private void genCenters(Point[] centers, Point[] pointArray) {
		for(int i = 0; i < k; i++){
			centers[i] = randomCenter(pointArray);
		}
		
	}

	/*
	 * Calculate the centroids of the data set
	 */
	private void findCenters(Point[] pointArray, Point[] centers, int[] c) {
			int i = 0;
			int j = 0;
			int l = 0;
			int test = 0;
			int cluster_size; 
			double min_dist = 0.0;
			double dist = 0.0;
			double sumx = 0.0;
			double sumy = 0.0;
			double xCheck = 0.0;
			double yCheck = 0.0;
			boolean finished = false;
			Point[] lastClusters = new Point[k];
			assignLastClusters(lastClusters, centers);
			
		   //for(l = 0; l < iters; l++){
			while(finished == false){
		         for(i = 0; i < m; i++){
		            min_dist = Double.MAX_VALUE;
		            for(j = 0; j < k; j++){
		               dist = distance(pointArray[i], centers[j]);
		               if(dist < min_dist){
		                  min_dist = dist;
		                  c[i] = j;
		               }
		            }
		         }	      

		         for(j = 0; j < k; j++){
		            sumx = 0.0;
		            sumy = 0.0;
		            cluster_size = 0;
		            for(i = 0; i < m; i++){
		               if(c[i] == j){
		                  sumx += pointArray[i].x;
		                  sumy += pointArray[i].y;
		                  cluster_size++;
		               }
		            }

		            if(cluster_size > 0){
		               centers[j].x = sumx/cluster_size;
		               centers[j].y = sumy/cluster_size;
		            } else{
		               centers[j] = randomCenter(pointArray);
		            }
		         }
		      // Check if cluster has changed
		      if(iters == -1){
		    	  for(i = 0; i < k; i++){
		    		  xCheck = Math.abs(lastClusters[i].x - centers[i].x);
		    		  yCheck = Math.abs(lastClusters[i].y - centers[i].y);
		    		  //System.out.println("xCheck: " + xCheck + " yCheck: " + yCheck);
		    		  if(xCheck > 1){
		    			  assignLastClusters(lastClusters, centers);
		    			  //System.out.println("Breaking at x");
		    			  break;
		    		  } else if (yCheck > 1){
		    			  assignLastClusters(lastClusters, centers);
		    			  //System.out.println("Breaking at y");
		    			  break;
		    		  }
		    	  }
		    	  
		    	  if(i == k) test++;
		      }
		      //System.out.println("Current i value is " + i);
		      if(i == k && test == 3) finished = true;
		   }
	}	
	


	private void assignLastClusters(Point[] lastClusters, Point[] centers) {
		for(int i = 0; i < k; i++)
			try {
				lastClusters[i] = centers[i].clone();
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	private Point randomCenter(Point[] pointArray2) {
		Random rand = new Random();
		Date d = new Date();
		try {
			return pointArray2[Math.abs((rand.nextInt() * d.getSeconds()) % m)].clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	double distance(Point point, Point center){
		double d = Math.sqrt(Math.pow((point.x - center.x), 2) + Math.pow((point.y - center.y), 2));
		return d;
	}
	
	public String toString()
	{
		return "K-Mean Implementation: k = " + this.k;
	}
	
	public void showGraphSepals()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("K-Means Chart");
		alert.setHeaderText("\tk = 3");
		//alert.setContentText("Could not find file blabla.txt!");
		
        final NumberAxis xAxis = new NumberAxis(4, 8, 1);
        final NumberAxis yAxis = new NumberAxis(1, 5, 1);        
        final ScatterChart<Number,Number> sc = new
            ScatterChart<>(xAxis,yAxis);
        xAxis.setLabel("Sepal Width");                
        yAxis.setLabel("Sepal Length");
        sc.setTitle("Sepal Graph");
        
        
        // set
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Iris-setosa");
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Iris-versicolor");
        XYChart.Series series3 = new XYChart.Series();
        series3.setName("Iris-virginica");
        XYChart.Series series4 = new XYChart.Series<>();
        series4.setName("Clusters");
        for(int i = 0; i < m; i++){
        	//System.out.println("Working...");
        	switch(type[i]){
        		case "Iris-setosa": 
        			series1.getData().add(new XYChart.Data(pointArray[i].x, pointArray[i].y));
        			break;
        		case "Iris-versicolor":
        			series2.getData().add(new XYChart.Data(pointArray[i].x, pointArray[i].y));
        			break;
        		case "Iris-virginica":
        			series3.getData().add(new XYChart.Data(pointArray[i].x, pointArray[i].y));
        			break;
        		default:
        	}
        }
        for(int i = 0; i < k; i++){
        	series4.getData().add(new XYChart.Data(centers[i].x, centers[i].y));
        }
        /*
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Iris-setosa");
        series1.getData().add(new XYChart.Data(4.2, 193.2));
        series1.getData().add(new XYChart.Data(2.8, 33.6));
        
        
        
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Iris-versicolor");
        series2.getData().add(new XYChart.Data(5.2, 229.2));
        series2.getData().add(new XYChart.Data(2.4, 37.6));
       
        
        XYChart.Series series3 = new XYChart.Series();
        series3.setName("Iris-virginica");
        series3.getData().add(new XYChart.Data(5.2, 229.2));
        series3.getData().add(new XYChart.Data(2.4, 37.6));
        */
        sc.getData().addAll(series1, series2, series3, series4);
        
        alert.setGraphic( sc );
		
		alert.show();
	}
	
	public void showGraphSepalsCluster()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("K-Means Chart");
		alert.setHeaderText("\tk = 3");
		//alert.setContentText("Could not find file blabla.txt!");
		
        final NumberAxis xAxis = new NumberAxis(4, 8, 1);
        final NumberAxis yAxis = new NumberAxis(1, 5, 1);        
        final ScatterChart<Number,Number> sc = new
            ScatterChart<>(xAxis,yAxis);
        xAxis.setLabel("Sepal Width");                
        yAxis.setLabel("Sepal Length");
        sc.setTitle("Sepal Clusters");
        
        
        // set
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Cluster 1");
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Cluster 2");
        XYChart.Series series3 = new XYChart.Series();
        series3.setName("Cluster 3");
        XYChart.Series series4 = new XYChart.Series<>();
        series4.setName("Cluster centers");
        for(int i = 0; i < m; i++){
        	//System.out.println("Working...");
        	switch(c[i]){
        		case 0: 
        			series1.getData().add(new XYChart.Data(pointArray[i].x, pointArray[i].y));
        			break;
        		case 1:
        			series2.getData().add(new XYChart.Data(pointArray[i].x, pointArray[i].y));
        			break;
        		case 2:
        			series3.getData().add(new XYChart.Data(pointArray[i].x, pointArray[i].y));
        			break;
        		default:
        	}
        }
        for(int i = 0; i < k; i++){
        	series4.getData().add(new XYChart.Data(centers[i].x, centers[i].y));
        }
        /*
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Iris-setosa");
        series1.getData().add(new XYChart.Data(4.2, 193.2));
        series1.getData().add(new XYChart.Data(2.8, 33.6));
        
        
        
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Iris-versicolor");
        series2.getData().add(new XYChart.Data(5.2, 229.2));
        series2.getData().add(new XYChart.Data(2.4, 37.6));
       
        
        XYChart.Series series3 = new XYChart.Series();
        series3.setName("Iris-virginica");
        series3.getData().add(new XYChart.Data(5.2, 229.2));
        series3.getData().add(new XYChart.Data(2.4, 37.6));
        */
        sc.getData().addAll(series1, series2, series3, series4);
        
        alert.setGraphic( sc );
		
		alert.show();
	}
	
	public void showGraphPetals()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("K-Means Chart");
		alert.setHeaderText("\tk = 3");
		//alert.setContentText("Could not find file blabla.txt!");
		// xAxis is left <-> right range, increment
		final NumberAxis xAxis = new NumberAxis(0, 8, 1);
        final NumberAxis yAxis = new NumberAxis(0, 5, 1);         
        final ScatterChart<Number,Number> sc = new
            ScatterChart<>(xAxis,yAxis);
        xAxis.setLabel("Petal Width");                
        yAxis.setLabel("Petal Length");
        sc.setTitle("Petal Graph");
        // set
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Iris-setosa");
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Iris-versicolor");
        XYChart.Series series3 = new XYChart.Series();
        series3.setName("Iris-virginica");
        XYChart.Series series4 = new XYChart.Series<>();
        series4.setName("Clusters");
        for(int i = 0; i < m; i++){
        	//System.out.println("Working...");
        	switch(type[i]){
        		case "Iris-setosa": 
        			series1.getData().add(new XYChart.Data(petalArray[i].x, petalArray[i].y));
        			break;
        		case "Iris-versicolor":
        			series2.getData().add(new XYChart.Data(petalArray[i].x, petalArray[i].y));
        			break;
        		case "Iris-virginica":
        			series3.getData().add(new XYChart.Data(petalArray[i].x, petalArray[i].y));
        			break;
        		default:
        	}
        }
        for(int i = 0; i < k; i++){
        	series4.getData().add(new XYChart.Data(petalCenters[i].x, petalCenters[i].y));
        }
        
        sc.getData().addAll(series1, series2, series3, series4);
        
        alert.setGraphic( sc );
		
		alert.show();
	}
	
	public void showGraphPetalsCluster()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("K-Means Chart");
		alert.setHeaderText("\tk = 3");
		//alert.setContentText("Could not find file blabla.txt!");
		// xAxis is left <-> right range, increment
		final NumberAxis xAxis = new NumberAxis(0, 8, 1);
        final NumberAxis yAxis = new NumberAxis(0, 5, 1);         
        final ScatterChart<Number,Number> sc = new
            ScatterChart<>(xAxis,yAxis);
        xAxis.setLabel("Petal Width");                
        yAxis.setLabel("Petal Length");
        sc.setTitle("Petal Clusters");
        // set
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Cluster 1");
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Cluster 2");
        XYChart.Series series3 = new XYChart.Series();
        series3.setName("Cluster 3");
        XYChart.Series series4 = new XYChart.Series<>();
        series4.setName("Cluster Centers");
        for(int i = 0; i < m; i++){
        	//System.out.println("Working...");
        	switch(petalC[i]){
        		case 0: 
        			series1.getData().add(new XYChart.Data(petalArray[i].x, petalArray[i].y));
        			break;
        		case 1:
        			series2.getData().add(new XYChart.Data(petalArray[i].x, petalArray[i].y));
        			break;
        		case 2:
        			series3.getData().add(new XYChart.Data(petalArray[i].x, petalArray[i].y));
        			break;
        		default:
        	}
        }
        for(int i = 0; i < k; i++){
        	series4.getData().add(new XYChart.Data(petalCenters[i].x, petalCenters[i].y));
        }
        
        sc.getData().addAll(series1, series2, series3, series4);
        
        alert.setGraphic( sc );
		
		alert.show();
	}
}