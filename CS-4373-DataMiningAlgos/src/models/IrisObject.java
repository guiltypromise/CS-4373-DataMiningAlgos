package models;

public class IrisObject 
{
	private double sepalWidth;
	private double sepalLength;
	private double petalWidth;
	private double petalLength;
	private double probSepalWidth;
	private double probSepalLength;
	private double probPetalWidth;
	private double probPetalLength;
	private String className;
	private String classifiedName;
	
	public IrisObject()
	{
		sepalWidth = 0;
		sepalLength = 0;
		petalWidth = 0;
		petalLength = 0;
		probSepalWidth = 0;
		probSepalLength = 0;
		probPetalWidth = 0;
		probPetalLength = 0;
		className = "";
	}

	public String getClassifiedName() {
		return classifiedName;
	}

	public void setClassifiedName(String classifiedName) {
		this.classifiedName = classifiedName;
	}

	/**
	 * @return the probSepalWidth
	 */
	public double getProbSepalWidth() {
		return probSepalWidth;
	}

	/**
	 * @param probSepalWidth the probSepalWidth to set
	 */
	public void setProbSepalWidth(double probSepalWidth) {
		this.probSepalWidth = probSepalWidth;
	}

	/**
	 * @return the probSepalLength
	 */
	public double getProbSepalLength() {
		return probSepalLength;
	}

	/**
	 * @param probSepalLength the probSepalLength to set
	 */
	public void setProbSepalLength(double probSepalLength) {
		this.probSepalLength = probSepalLength;
	}

	/**
	 * @return the probPetalWidth
	 */
	public double getProbPetalWidth() {
		return probPetalWidth;
	}

	/**
	 * @param probPetalWidth the probPetalWidth to set
	 */
	public void setProbPetalWidth(double probPetalWidth) {
		this.probPetalWidth = probPetalWidth;
	}

	/**
	 * @return the probPetalLength
	 */
	public double getProbPetalLength() {
		return probPetalLength;
	}

	/**
	 * @param probPetalLength the probPetalLength to set
	 */
	public void setProbPetalLength(double probPetalLength) {
		this.probPetalLength = probPetalLength;
	}

	public double getSepalWidth() {
		return sepalWidth;
	}

	public void setSepalWidth(double sepalWidth) {
		this.sepalWidth = sepalWidth;
	}

	public double getSepalLength() {
		return sepalLength;
	}

	public void setSepalLength(double sepalLength) {
		this.sepalLength = sepalLength;
	}

	public double getPetalWidth() {
		return petalWidth;
	}

	public void setPetalWidth(double petalWidth) {
		this.petalWidth = petalWidth;
	}

	public double getPetalLength() {
		return petalLength;
	}

	public void setPetalLength(double petalLength) {
		this.petalLength = petalLength;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String toString()
	{
		String out = String.format("Sepal L: %.1f, Sepal W: %.1f, Petal L: %.1f, Petal W: %.1f, Class: %s"
				, sepalLength
				, sepalWidth
				, petalLength
				, petalWidth
				, className
				);
		return out;
	}
}
