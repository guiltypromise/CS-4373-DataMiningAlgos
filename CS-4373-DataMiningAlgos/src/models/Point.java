package models;
/*
 * Used for specific points in a data set.
 * For now will be used in k-means algorithm
 */
public class Point implements Cloneable{
	public double x;
	public double y;
	
	public Point(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	public Point clone() throws CloneNotSupportedException
    {
        return (Point) super.clone();
    }
	
}
