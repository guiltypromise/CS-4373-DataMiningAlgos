package models;

import java.util.ArrayList;

public class IrisClass
{
	String name;
	private double probability;
	private int total;
	public int getTotal() {
		return total;
	}


	public void setTotal(int total) {
		this.total = total;
	}

	private double meanSepalWidth;
	private double meanSepalLength;
	private double meanPetalWidth;
	private double meanPetalLength;
	private double stdSepalWidth;
	private double stdSepalLength;
	private double stdPetalWidth;
	private double stdPetalLength;
	private ArrayList<IrisObject> data;
	
	public IrisClass(String name) 
	{
		this.name = name;
		probability = 0.33;
		meanSepalWidth = 0.0;
		meanSepalLength= 0.0;
		meanPetalWidth = 0.0;
		meanPetalLength = 0.0;
		stdSepalWidth = 0.0;
		stdSepalLength = 0.0;
		stdPetalWidth = 0.0;
		stdPetalLength = 0.0;
		data = new ArrayList<IrisObject>();
	}
	
	public void addToCount()
	{
		total += 1;
	}

	/**
	 * @return the meanSepalWidth
	 */
	public double getMeanSepalWidth() {
		return meanSepalWidth;
	}

	/**
	 * @return the meanSepalLength
	 */
	public double getMeanSepalLength() {
		return meanSepalLength;
	}

	/**
	 * @return the meanPetalWidth
	 */
	public double getMeanPetalWidth() {
		return meanPetalWidth;
	}

	/**
	 * @return the meanPetalLength
	 */
	public double getMeanPetalLength() {
		return meanPetalLength;
	}

	/**
	 * @return the stdSepalWidth
	 */
	public double getStdSepalWidth() {
		return stdSepalWidth;
	}

	/**
	 * @return the stdSepalLength
	 */
	public double getStdSepalLength() {
		return stdSepalLength;
	}

	/**
	 * @return the stdPetalWidth
	 */
	public double getStdPetalWidth() {
		return stdPetalWidth;
	}

	/**
	 * @return the stdPetalLength
	 */
	public double getStdPetalLength() {
		return stdPetalLength;
	}

	/**
	 * Calculates the mean and std deviation for each of the variables
	 * of this Iris class.
	 */
	public void calculateMeans()
	{
		int totalRecords = data.size();
		double sum = 0;
		
		// calculate sepal width averages
		for( int i = 0; i < totalRecords; i++ )
		{
			//System.out.println(data.get(i).getSepalWidth());
			sum += data.get(i).getSepalWidth();
		}
		this.meanSepalWidth = (double) (sum / totalRecords) ;
//		System.out.printf("[%s] Sepal Width mean: %.3f\n"
//				, name
//				, meanSepalWidth
//		);
		
		sum = 0.0; // reset the sum
		
		// calculate sepal length averages
		for( int i = 0; i < totalRecords; i++ )
		{
			//System.out.println(data.get(i).getSepalWidth());
			sum += data.get(i).getSepalLength();
		}
		this.meanSepalLength = (double) (sum / totalRecords) ;
//		System.out.printf("[%s] Sepal Length mean: %.3f\n"
//				, name
//				, meanSepalLength
//		);
		
		sum = 0.0; // reset the sum

		// calculate petal Width averages
		for( int i = 0; i < totalRecords; i++ )
		{
			//System.out.println(data.get(i).getSepalWidth());
			sum += data.get(i).getPetalWidth();
		}
		this.meanPetalWidth = (double) (sum / totalRecords) ;
//		System.out.printf("[%s] Petal Width mean: %.3f\n"
//				, name
//				, meanPetalWidth
//		);
		
		sum = 0.0; // reset the sum

		// calculate petal length averages
		for( int i = 0; i < totalRecords; i++ )
		{
			//System.out.println(data.get(i).getSepalWidth());
			sum += data.get(i).getPetalLength();
		}
		this.meanPetalLength = (double) (sum / totalRecords) ;
//		System.out.printf("[%s] Petal Length mean: %.3f\n"
//				, name
//				, meanPetalLength
//		);
	}

	// assumes we have the means of all the class variables
	public void calculateStdDevs()
	{
		int totalRecords = data.size();
		double variance = 0.0;
		double meanDiff = 0.0;
		double squaredSum = 0.0;
		
		// for each sepal width value
		for( int i = 0; i < totalRecords; i++ )
		{
			// subtract from the mean 
			meanDiff = data.get(i).getSepalWidth() - meanSepalWidth;
			
			// square it ( multiply twice )
			meanDiff = Math.pow(meanDiff, 2);
			
			// add to squared sum
			squaredSum += meanDiff;
		}
		
		// get variance for sepalWidth
		variance = (double) squaredSum / totalRecords;
		
		// get standard dev
		this.stdSepalWidth = Math.sqrt(variance);

//		System.out.printf("[%s] Sepal Width Std Dev: %.4f\n"
//				, name
//				, stdSepalWidth
//		);

		
		// reset values
		variance = 0.0;
		meanDiff = 0.0;
		squaredSum = 0.0;
		
		// for sepal length
		for( int i = 0; i < totalRecords; i++ )
		{
			// subtract from the mean 
			meanDiff = data.get(i).getSepalLength() - meanSepalLength;
			
			// square it ( multiply twice )
			meanDiff = Math.pow(meanDiff, 2);
			
			// add to squared sum
			squaredSum += meanDiff;
		}
		
		// get variance for sepalWidth
		variance = (double) squaredSum / totalRecords;
		
		// get standard dev
		this.stdSepalLength = Math.sqrt(variance);

//		System.out.printf("[%s] Sepal Length Std Dev: %.4f\n"
//				, name
//				, stdSepalLength
//		);
		
		// reset values
		variance = 0.0;
		meanDiff = 0.0;
		squaredSum = 0.0;
		
		// for petal width
		for( int i = 0; i < totalRecords; i++ )
		{
			// subtract from the mean 
			meanDiff = data.get(i).getPetalWidth() - meanPetalWidth;
			
			// square it ( multiply twice )
			meanDiff = Math.pow(meanDiff, 2);
			
			// add to squared sum
			squaredSum += meanDiff;
		}
		
		// get variance for sepalWidth
		variance = (double) squaredSum / totalRecords;
		
		// get standard dev
		this.stdPetalWidth = Math.sqrt(variance);

//		System.out.printf("[%s] Petal Width Std Dev: %.4f\n"
//				, name
//				, stdPetalWidth
//		);
		
		// reset values
		variance = 0.0;
		meanDiff = 0.0;
		squaredSum = 0.0;
		
		// for petal length
		for( int i = 0; i < totalRecords; i++ )
		{
			// subtract from the mean 
			meanDiff = data.get(i).getPetalLength() - meanPetalLength;
			
			// square it ( multiply twice )
			meanDiff = Math.pow(meanDiff, 2);
			
			// add to squared sum
			squaredSum += meanDiff;
		}
		
		// get variance for sepalWidth
		variance = (double) squaredSum / totalRecords;
		
		// get standard dev
		this.stdPetalLength = Math.sqrt(variance);

//		System.out.printf("[%s] Petal Length Std Dev: %.4f\n"
//				, name
//				, stdPetalLength
//		);
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public double getProbability() {
		return probability;
	}

	public void setProbability(double probability) {
		this.probability = probability;
	}

	/**
	 * @return the data
	 */
	public ArrayList<IrisObject> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(ArrayList<IrisObject> data) {
		this.data = data;
	}
	
	public void addData( IrisObject data )
	{
		this.data.add(data);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() 
	{
		String out = String.format("%s:"
				+ "\nSepalLength  mean: %.3f std");
		return "Class=" + name + ", data=" + data + "]\n";
	}
}
