CS-4373 Data Mining Algorithms README

#Stand Alone Installation:
1. Extract the "Team 07 Algo Demo JAR" folder.
2. Locate the JAR type file.
3. Double-click on JAR to run application.

#Eclipse Project Execution:
1. Located the source files as an archived file.
2. Import in Eclipse following 'Import' steps:
	i. Import -> General -> Existing Projects into Workspace -> Archive: <project file.zip> -> Next -> Next.
3. Open Driver.java under /src/driver in Package Directory.
4. Click 'Run' button in Eclipse toolbar.
	